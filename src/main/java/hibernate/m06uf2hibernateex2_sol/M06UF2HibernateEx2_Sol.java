/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package hibernate.m06uf2hibernateex2_sol;

import entitats.Customer;
import com.github.javafaker.Faker;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author victor
 */
public class M06UF2HibernateEx2_Sol {
    private static SessionFactory factory;
    private static Session session;
    private static final List llistaObjectes = new ArrayList<>();
    private static final Integer maxObjectes = 1000;
    private static Faker faker = new Faker();

    public static void main(String[] args) {
        try
        {
            
            factory = new Configuration().configure().buildSessionFactory();
            
            System.out.println("Iniciem sessió...");
            session = factory.openSession();
            
            System.out.println("Iniciem transaccio...");
            session.getTransaction().begin();
            
            System.out.println("Creem " + maxObjectes + " objectes amb estrategia una sola transacció per a tots:");
            for (int i = 0; i < maxObjectes; i ++)
            {
                String dni = faker.numerify("########") + faker.letterify("?",true);
                String customerName = faker.name().name();
                String contactLastName = faker.name().lastName();
                String contactFirstName = faker.name().firstName();
                String addressLine1 = faker.address().streetAddress();
                String addressLine2 = faker.address().secondaryAddress();
                String phone = faker.phoneNumber().cellPhone();
                String city = faker.address().cityName();
                String state = faker.address().state();
                String postalCode = faker.address().zipCode();
                String country = faker.address().country(); if(country.length() > 25) country = country.substring(0, 25);

                Integer salesRepEmployeeNumber = faker.number().randomDigit();
                float creditLimit = (float)faker.number().randomDouble(2, 0, 9999);


                llistaObjectes.add(new Customer(dni, customerName, contactLastName, contactFirstName, phone, addressLine1, addressLine2, city, state, postalCode, country, salesRepEmployeeNumber, creditLimit));
            }
            
            System.out.println("Persistim l'estat dels objectes");
            llistaObjectes.forEach(x -> session.persist(x));
            
            System.out.println("Desem a BBDD...");
            session.getTransaction().commit();
            System.out.println("Fet !!");
            

        
        } catch (ConstraintViolationException ex){
             if (session.getTransaction() !=null) 
                 session.getTransaction().rollback();
             
             System.out.println("Violació de la restricció: " + ex.getMessage());
            
        } catch (HibernateException ex) {
            if (session.getTransaction()!=null) 
                session.getTransaction().rollback();
            
            System.out.println("Excepció d'hibernate: " + ex.getMessage());
      } 
        
        catch (Exception ex) {
            if (session.getTransaction()!=null) 
                session.getTransaction().rollback();
            
            System.out.println("Excepció d'hibernate: " + ex.getMessage());
      } 
        
        finally {
            
         //Tanquem la sessió
         session.close();
         
         //Finalitzem Hibernate
         factory.close();
      }
    }
}
